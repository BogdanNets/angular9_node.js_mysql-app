import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    EmployeeListComponent,
    LogInComponent,
    RegisterComponent,
} from './workflow';
import { AuthGuard } from './shared/guards/auth.guard';
import { RoutingConstants } from './shared/constants/routing.constants';

const routes: Routes = [
    {
        path: RoutingConstants.EMPLOYEE_LIST,
        component: EmployeeListComponent,
        canActivate: [AuthGuard],
    },
    { path: RoutingConstants.LOGIN, component: LogInComponent },
    { path: RoutingConstants.REGISTER, component: RegisterComponent },
    { path: '', pathMatch: 'full', redirectTo: RoutingConstants.EMPLOYEE_LIST },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
