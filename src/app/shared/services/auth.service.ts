import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RegistrationData } from '../models/registrationData.model';
import { LoginData } from '../models/loginData.model';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private _baseRequestURL: string = 'http://localhost:3000/auth';

    constructor(private _http: HttpClient, private _router: Router) {}
    
    registerUser(registrationData: RegistrationData): void {
        this._http.post<RegistrationData>(`${this._baseRequestURL}/register`, registrationData).subscribe(data => {});
    }

    loginUser(loginData: LoginData): void {
        this._http.post<LoginData>(`${this._baseRequestURL}/login`, loginData).subscribe( (data: LoginData) => {
            localStorage.setItem('token', data.token);
            this._router.navigate(['/employeesList']);
        });
    }

    loggedIn(): boolean {
        return !!localStorage.getItem('token');
    }

    getToken(): string {
        return localStorage.getItem('token');
    }
}
