import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { share } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {

    visibility: BehaviorSubject<boolean>;

    constructor() {
        this.visibility = new BehaviorSubject(false);
    }

    show(): void {
        this.visibility.next(true);
    }

    hide(): void {
        this.visibility.next(false);
    }

    isVisible(): Observable<boolean> {
        return this.visibility.asObservable().pipe(share());
    }
}