import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

import { MaterialModule } from './material.module';
import { EmployeeViewComponent } from '../workflow';

@NgModule({
    imports: [MaterialModule, ReactiveFormsModule],
    declarations: [],
    exports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
    ],
    providers: [],
    entryComponents: [EmployeeViewComponent],
})
export class SharedModule { }
