import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeData } from 'src/app/shared/models/employee.model';

@Component({
    selector: 'app-employee-view',
    templateUrl: './employee-view.component.html',
    styleUrls: ['./employee-view.component.scss'],
})
export class EmployeeViewComponent implements OnInit {
    dataSource: EmployeeData = new EmployeeData();

    constructor(@Inject(MAT_DIALOG_DATA) public data: EmployeeData) {}

    ngOnInit(): void {
        this.dataSource = this.data;
    }
}
