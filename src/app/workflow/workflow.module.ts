import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeEditComponent } from './components/employee-edit/employee-edit.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { RegisterComponent } from './components/register/register.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EmployeeViewComponent } from './components/employee-view/employee-view.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [SharedModule, MatFormFieldModule, RouterModule],
    declarations: [
        EmployeeListComponent,
        EmployeeEditComponent,
        LogInComponent,
        RegisterComponent,
        EmployeeViewComponent,
    ],
    entryComponents: [EmployeeViewComponent, EmployeeEditComponent],
})
export class WorkflowModule { }
