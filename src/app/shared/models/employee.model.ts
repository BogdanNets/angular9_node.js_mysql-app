export class EmployeeData {
    public _id?: string;
    public empId?: number;
    public empName?: string;
    public empActive?: number; // there's no boolean type in mysql
    public emp_dpID?: string;
    public dpID?: string;
    public dpName?: string;
}
