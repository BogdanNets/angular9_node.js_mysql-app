import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { EmployeeData } from '../models/employee.model';

@Injectable({
	providedIn: 'root',
})
export class ApiService {
	private _baseRequestURL: string = 'http://localhost:3000';

	constructor(private _http: HttpClient) { }

	get(): Observable<EmployeeData[]> {
		return this._http.get<EmployeeData[]>(`${this._baseRequestURL}/employeesList`);
	}

	put(data: EmployeeData): Observable<EmployeeData> {
		const body = data;
		return this._http.put<EmployeeData>(`${this._baseRequestURL}/update/${data.empId}`, body);
	}

	delete(empId: number): Observable<number> {
		return this._http.delete<number>(`${this._baseRequestURL}/delete/${empId}`);
	}
}
