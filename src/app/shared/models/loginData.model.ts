export class LoginData {
    public email?: string;
    public password?: string;
    public token?: string;
}
