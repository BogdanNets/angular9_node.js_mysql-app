import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { SpinnerService } from './shared/services';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
    title = 'front-end';
    displaySpinner: boolean;

    constructor(public spinner: SpinnerService, private _cdr: ChangeDetectorRef) { }

    ngAfterViewInit(): void {
        this.spinner.isVisible().subscribe(res => {
            this.displaySpinner = res;
            this._cdr.detectChanges();
        })
    }

}
