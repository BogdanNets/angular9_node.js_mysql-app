import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { RoutingConstants } from 'src/app/shared/constants/routing.constants';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
    logoutEnable: boolean = false;
    registerEnable: boolean = false;
    loginEnable: boolean = false;

    constructor(private router: Router) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.loginEnable = event.url.includes(RoutingConstants.REGISTER);
                this.logoutEnable = event.url.includes(RoutingConstants.EMPLOYEE_LIST);
                this.registerEnable = event.url.includes(RoutingConstants.LOGIN);
            }
        });
    }

    logOut(): void {
        localStorage.removeItem('token');
    }
}
