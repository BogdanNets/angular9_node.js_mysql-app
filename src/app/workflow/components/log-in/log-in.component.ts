import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RegistrationData } from 'src/app/shared/models/registrationData.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { RoutingConstants } from 'src/app/shared/constants/routing.constants';

@Component({
    selector: 'app-log-in',
    templateUrl: './log-in.component.html',
    styleUrls: ['./log-in.component.scss'],
    host: { class: 'auth-component' }
})
export class LogInComponent implements OnInit {
    loginData: RegistrationData = new RegistrationData();

    constructor(private _authService: AuthService, private _router: Router) { }

    ngOnInit(): void {
        if (this._authService.loggedIn()) {
            this._router.navigate([`/${RoutingConstants.EMPLOYEE_LIST}`]);
        }
    }

    form: FormGroup = new FormGroup({
        username: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required]),
    });

    submit(): void {
        if (this.form.valid) {
            this.loginData.email = this.form.get('username').value;
            this.loginData.password = this.form.get('password').value;

            this._authService.loginUser(this.loginData);
        }
    }
}
