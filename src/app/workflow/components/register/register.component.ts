import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RegistrationData } from 'src/app/shared/models/registrationData.model';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    host: { class: 'auth-component' },
})
export class RegisterComponent implements OnInit {
    registrationData: RegistrationData = new RegistrationData();

    constructor(private authService: AuthService, private _router: Router) { }

    ngOnInit(): void { }

    form: FormGroup = new FormGroup({
        username: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required]),
    });

    submit(): void {
        if (this.form.valid) {
            this.registrationData.email = this.form.get('username').value;
            this.registrationData.password = this.form.get('password').value;

            this.authService.registerUser(this.registrationData);
            this._router.navigate(['/login']);
        }
    }
}
