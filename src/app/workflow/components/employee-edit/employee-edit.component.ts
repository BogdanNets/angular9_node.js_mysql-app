import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ApiService } from 'src/app/shared/services';
import { EmployeeData } from 'src/app/shared/models/employee.model';

@Component({
    selector: 'app-employee-edit',
    templateUrl: './employee-edit.component.html',
    styleUrls: ['./employee-edit.component.scss'],
})
export class EmployeeEditComponent implements OnInit {
    form: FormGroup;
    dataSource: EmployeeData = new EmployeeData();
    public dialog: MatDialog;


    constructor(
        @Inject(MAT_DIALOG_DATA) public data: EmployeeData,
        private _apiService: ApiService
    ) { }

    ngOnInit(): void {
        this.dataSource = this.data;
        this.initializeFormGroup();
    }

    initializeFormGroup(): void {
        console.log(this.dataSource.empActive);
        console.log(typeof this.dataSource.empActive);
        this.form = new FormGroup({
            controlEmployeeName: new FormControl(this.dataSource.empName, [Validators.required]),
            controlEmployeeActive: new FormControl(this.dataSource.empActive ? true : false, [Validators.required]),
            controlEmployeeDepartment: new FormControl(this.dataSource.dpName, [Validators.required]),
        });
    }

    submit(): void {
        if (this.form.valid) {
            this.dataSource.empName = this.form.get('controlEmployeeName').value;
            this.dataSource.empActive = this.form.get('controlEmployeeActive').value ? 1 : 0;
            this.dataSource.dpName = this.form.get('controlEmployeeDepartment').value;

            console.log(this.dataSource.empActive);
            console.log(typeof this.dataSource.empActive);


            this._apiService.put(this.dataSource).subscribe(res => { });
        }
    }
}
