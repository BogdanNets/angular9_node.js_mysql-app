import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs";

import { AuthService } from './auth.service';
import { SpinnerService } from './spinner.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
    constructor(
        private _auth: AuthService,
        private spinnerService: SpinnerService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinnerService.show();

        let tokenizedReq = req.clone({
            setHeaders: {
                Authorization: `Bearer ${this._auth.getToken()}`,
            }
        });

        return next.handle(tokenizedReq).pipe(
            tap(event => {
                debugger
                if (event instanceof HttpResponse || event.type === 0) {
                    setTimeout(() => {
                        this.spinnerService.hide();
                    }, 500);
                }
            })
        );;
    }
}