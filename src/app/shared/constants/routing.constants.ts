export class RoutingConstants {
    static EMPLOYEE_LIST = "employeesList";
    static LOGIN = "login";
    static REGISTER = "register";
}
