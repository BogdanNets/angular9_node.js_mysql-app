import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { ApiService } from 'src/app/shared/services';
import { EmployeeData } from 'src/app/shared/models/employee.model';
import { EmployeeViewComponent } from '../employee-view/employee-view.component';
import { EmployeeEditComponent } from '../employee-edit';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss'],
    host: { class: 'w-100' },
})
export class EmployeeListComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    displayedColumns: string[] = [
        'empId',
        'empName',
        'empActive',
        'empDepartment',
        'view',
        'edit',
        'delete',
    ];
    dataSource = new MatTableDataSource<EmployeeData>();
    routeQueryParams: Subscription;

    constructor(
        private _apiService: ApiService,
        private _router: Router,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        this._apiService.get().subscribe(
            empData => {
                this.dataSource = new MatTableDataSource<EmployeeData>(empData);
                this.dataSource.paginator = this.paginator;
            },
            err => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401 || err.status === 500) {
                        localStorage.removeItem('token');
                        this._router.navigate(['/login']);
                    }
                }
            }
        );
    }

    applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    deleteEmployee(empId: number) {
        this._apiService.delete(empId).subscribe((err) => {
            if (!err) {
                this._apiService.get().subscribe((res) => {
                    this.dataSource = new MatTableDataSource<EmployeeData>(res);
                    this.dataSource.paginator = this.paginator;
                });
            }
        });
    }

    openViewDialog(data: EmployeeData): void {
        let dialogRef = this.dialog.open(EmployeeViewComponent, {
            data,
        });

        dialogRef.afterClosed().subscribe((result) => { });
    }

    openEditDialog(data: EmployeeData): void {
        let dialogRef = this.dialog.open(EmployeeEditComponent, {
            data,
        });

        dialogRef.afterClosed().subscribe((result) => { });
    }
}
